<?php
    namespace App\Utilities;

    /**
     * Convert xml to json
     * 
     * @author Carlos Alexander Lemus Guardado <alexander.guardado1988@gmail.com>
     */
    class XmlConvert {
        /**
         * Create all brach from json
         *
         * @param DOMDocument $xml
         * @param DOMElement $xml_father
         * @param array $json
         * @return void
         */
        private static function getBranchFromJson($xml, $xml_father, $json){
            // Get all branch
            foreach ($json as $key => $values) {
                // Validate if it is a json or a value
                if (is_array($values)) { // Is Json
                    $xml_branch = $xml->createElement( $key ); // create new Branch
                    self::getBranchFromJson($xml, $xml_branch, $values); // Get values of the branch
                } else // Save in brach
                    $xml_branch = $xml->createElement( $key, htmlspecialchars($values) );
                // Save the branch
                $xml_father->appendChild($xml_branch);
            }
        }

        /**
         * Create XML from json
         *
         * @param array $json
         * @param string $title
         * @return string
         */
        public static function fromJson($json, $title = "root") {
            // Dom xml
            $xml = new \DOMDocument( "1.0", "ISO-8859-15" );
            // Name of the root
            $xml_root = $xml->createElement($title);
            // Create all branchs            
            self::getBranchFromJson($xml, $xml_root, $json);
            // Add the root to xml
            $xml->appendChild($xml_root);
            // Return the xml
            return $xml->saveXML();
        }

    }