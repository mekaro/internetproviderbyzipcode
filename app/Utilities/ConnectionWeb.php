<?php

    namespace App\Utilities;

    /**
     * Control of connection with web site
     * 
     * @author Carlos Alexander Lemus Guardado <alexander.guardado1988@gmail.com>
     */
    class ConnectionWeb {
        // static functions --------------------------------------------------------------------------------
        // static public functions -------------------------------------------------------------------------
        /**
         * Generate header using data
         *
         * @param array $data
         * @return void
         */
        public static function generateHeaders($data) {
            return implode('', array_map(function($key, $value) {
                    return "$key: $value\r\n";
                }, array_keys($data), array_values($data))
            );
        }
        
        /**
         * Get content using the URL
         *
         * @param string $url
         * @param array $data
         * @param string $type
         * @return string
         */
        public static function getContentByUrl($url, $data = [], $type = "GET"){
            // Check type of connection
            if ($type == "GET")
                return file_get_contents($url . ( (count($data) > 0)? ("?". http_build_query($data)) : "") ); // Return content of Get
            else {
                // Create object curl for create post conection
                $curl = curl_init();
                // Define properties
                curl_setopt_array($curl, array(
                    CURLOPT_URL => $url,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30000,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => $type,
                    CURLOPT_POSTFIELDS => json_encode($data),
                    CURLOPT_HTTPHEADER => array(
                        "accept: */*",
                        "accept-language: en-US,en;q=0.8",
                        "content-type: application/json",
                    ),
                ));
                // return the content
                $content = curl_exec($curl);
                // Close conec
                curl_close($curl);
                // return the content
                return $content;
            }
        }
    }