<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class ProvidersAddressRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address' => 'max:100|regex:/^[a-zA-Z0-9,+ ]+$/',
            'format' => [Rule::in(['json', 'xml'])],
            'dbaname' => 'nullable|regex:/^[a-zA-Z0-9,+ ]+$/',
            'techcode' => 'numeric',
            'consumer' => 'boolean',
            'business' => 'boolean',
        ];
    }
}
