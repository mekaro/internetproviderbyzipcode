<?php

namespace App\Http\Controllers;

use App\Utilities\MapFind;
use Illuminate\Http\Request;
use App\Utilities\XmlConvert;
use App\Http\Requests\ProvidersRequest;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Requests\ProvidersAddressRequest;
use phpDocumentor\Reflection\Types\Boolean;

class ProviderController extends Controller
{
    // Constants ---------------------------------------------------------------------
    /**
     * Connection type constants
     */
    public const TYPES_CONNECTION = [
        0 => 'All Other',
        10 => 'Asymmetric xDSL',
        11 => 'ADSL2, ADSL2+',
        12 => 'VDSL',
        20 => 'Symmetric xDSL*',
        30 => 'Other Copper Wireline',
        40 => 'Cable Modem other than DOCSIS 1, 1.1, 2.0, 3.0, or 3.1',
        41 => 'Cable Modem – DOCSIS 1, 1.1 or 2.0',
        42 => 'Cable Modem – DOCSIS 3.0',
        43 => 'Cable Modem – DOCSIS 3.1',
        50 => 'Optical Carrier / Fiber to the end user',
        60 => 'Satellite',
        70 => 'Terrestrial Fixed Wireless',
        90 => 'Electric Power Line'
    ];

    // Static functions --------------------------------------------------------------------------------------
    // Static private functions ------------------------------------------------------------------------------
    /**
     * Generate list of provider using request, location and fcc data
     *
     * @param array $request
     * @param array $location
     * @param array $fcc
     * @return Response
     */
    public static function getProvidersData($request, $location = [], $fcc = []) {
        // Array of filters
        $filter = [
            "techcode" => $request['techcode'],
            "dbaname" => $request['dbaname'], 
            "consumer" => $request['consumer'], 
            "business" => $request['business']
        ];
        // Add Blockcode
        if(array_key_exists("blockcode", $fcc)) $filter["blockcode"] = $fcc['blockcode'];
        // Get all providers
        $providers = self::getProviders($filter);
        // Create information data
        $information = [];
        // Join all information
        if ( count($location) > 0 ) $information['location'] = $location;
        if ( count($fcc) > 0 ) $information['fcc'] = $fcc;
        if ( count($providers) > 0 ) $information['providers'] = $providers;
        // Response
        if (isset($request->format))
            switch ($request->format) {
                case 'xml':// Create file xml and main tag
                    return response(
                        XmlConvert::fromJson($information, "internet_provaiders"), 
                        200, 
                        [ 'Content-Type' => 'application/xml']
                    );
            }
        // Return JSON by default
        return response($information, 200);
    }
    /**
     * Get providers By url
     *
     * @param string $url
     * @return array
     */
    private static function getProvidersByUrl($url){
        // Get Providers
        $providers = [];
        $result_providers = json_decode(file_get_contents($url), true);
        // Foreach with type of connection
        for ($index=0; $index < count($result_providers); $index++) { 
            $provider = $result_providers[$index];
            $provider['tech'] = self::TYPES_CONNECTION[$provider['techcode']];
            $providers["provider_".$provider['provider_id']] = $provider;
        }
        // Return all providers
        return $providers;
    }

    /**
     * List of providers type satelital
     *
     * @param int $techcode
     * @return array
     */
    private static function getProvidersSatelitals($techcode){
        // Url of the providers only satelital code
        $providers_url = "https://opendata.fcc.gov/resource/s7ac-qb2r.json?%24%24app_token=".env('FCC_TOKEN', '')."%20AND%20techcode=%22$techcode%22";
        // Return all providers
        return self::getProvidersByUrl($providers_url);
    }

    /**
     * Get list of providers
     *
     * @param array $data
     * @return void
     */
    private static function getProviders($data = []){
        // base url for providers
        $providers_url = "https://opendata.fcc.gov/resource/s7ac-qb2r.json?%24%24app_token=".env('FCC_TOKEN', '');
        // Check if data is empty
        if (count($data) > 0) {
            // Create url
            $providers_url .= "&%24WHERE=";
            // If first element to add
            $isFirst = true;
            // Check consumer and business
            if ( (isset($data["consumer"]) && isset($data["business"]) ) && (!$data["consumer"] && !$data["business"]) )
                $data = array_diff_key($data, ["consumer" => "", "business" => ""]);
            // Get data with key and value
            foreach ($data as $key => $value) {
                // if empty next step
                if (!isset($value) || $value == "-1" || $value == "") continue;
                // Separate the first and other
                $providers_url .= ( ($isFirst)? "$key" : "%20AND%20$key");
                // Check type of element
                if (in_array($key,[ "blockcode", "techcode" ]))
                    $providers_url .= "=%22$value%22";
                else if ($key == "dbaname")
                    $providers_url .= "%20like%20%22%25$value%25%22";
                else if (in_array($key,[ "consumer", "business" ]))
                    $providers_url .= "=$value";;
                // Finish 
                $isFirst = false;
            }
        }
        // Return providers
        return self::getProvidersByUrl($providers_url);
    }

    /**
     * Generate provider for location and request
     *
     * @param array $location
     * @param Request $request
     * @return Response
     */
    private static function generateProviders($location, $request)
    {
        // Verify integrity
        if ($location['status'] != "OK") return response($location['cause'] , 404);
        // Obtainig the census block
        $fcc = MapFind::getCensusBlock($location['latitude'], $location['longitude']);
        // Verify integrity
        if ($fcc['status'] != "OK") return response($fcc['cause'] , 404);
        // Return providers
        return self::getProvidersData($request, $location, $fcc);
    }

    // Functions used with the router -------------------------------------------------------------------
    /**
     * Get all Providers using the location
     *
     * @param ProvidersAddressRequest $request
     * @return Response
     */
    public function getProvidersByLocation(ProvidersAddressRequest $request) {
        // Return provider using the zipcode
        return $this->generateProviders(MapFind::getCoordinates($request['address'], 'address'), $request);
    }

    /**
     * Get all providers using the zipcode
     *
     * @param string $code
     * @param ProviderRequest $request
     * @return Response
     */
    public function getProvidersByZipcode($code, ProvidersRequest $request)
    {
        // Return provider using the zipcode
        return self::generateProviders(MapFind::getCoordinates($code), $request);
    }

    /**
     * Get providers using my current location
     *
     * @param ProvidersRequest $request
     * @return Response
     */
    public function getProvidersByCurrentLocation(ProvidersRequest $request){
        // Return my current location
        return self::generateProviders(MapFind::getCurrentCoordinates(), $request);
    }

    /**
     * Return all providers onlyne filters
     *
     * @param ProvidersRequest $request
     * @return Response
     */
    public function getAllProviders(ProvidersRequest $request) {
        return self::getProvidersData($request);
    }
}
