<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class ProvidersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'format' => [Rule::in(['json', 'xml'])],
            'dbaname' => 'nullable|regex:/^[a-zA-Z0-9,+ ]+$/',
            'techcode' => 'numeric',
            'consumer' => 'boolean',
            'business' => 'boolean',
        ];
    }
}
