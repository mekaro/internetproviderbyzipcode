<?php

return [
    'error_location_zipcode' => 'The zipcode was not found.',
    'error_location_addres' => 'The address was not found.',
    'error_census_block' => 'The census block was not found',
    'error_current_location' => 'The location could not be obtained',
];