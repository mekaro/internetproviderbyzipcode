<?php

return [
    'error_location_zipcode' => 'No se ha encontrado el zip code.',
    'error_location_addres' => 'No se ha encontrado la direccion.',
    'error_census_block' => 'No se ha encontrado el bloque de censo',
    'error_current_location' => 'No se pudo obtener la ubicación',
];