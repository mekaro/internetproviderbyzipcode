<?php 
    namespace App\Utilities;

    use App\Utilities\ConnectionWeb;

    /**
     * Find Locations, targets and providers
     * 
     * @author Carlos Alexander Lemus Guardado <alexander.guardado1988@gmail.com>
     */
    class MapFind {
        // static functions --------------------------------------------------------------------------------
        // static private functions ------------------------------------------------------------------------
        /**
         * Return coordinates using the api google
         *
         * @param string $google_map_url
         * @param string $request_type
         * @param array $data
         * @return array
         */
        private static function getCoordinatesByUrl($google_map_url, $type, $request_type = "GET", $data = []){
            // Get content of URL
            $content = ConnectionWeb::getContentByUrl($google_map_url, $data, $request_type);
            // Result of the request
            $map_result = json_decode($content, true);
            // Check status of the request
            if ($map_result['status'] == 'OK') {
                // Get and save information
                $geometry_location = $map_result['results'][0]['geometry']['location'];
                $location = [
                    "status" => "OK",
                    "latitude" => (isset($geometry_location['lat']) ? $geometry_location['lat'] : ""),
                    "longitude" => (isset($geometry_location['lng']) ? $geometry_location['lng'] : ""),
                    "formatted_address" => (isset($map_result['results'][0]['formatted_address']) ? $map_result['results'][0]['formatted_address'] : ""),
                ];
                // Otaining address
                $address_components = $map_result['results'][0]['address_components'];
                foreach ($address_components as $address) {
                    // If possible get the name of the state
                    if ($address['types'][0] == "administrative_area_level_1")
                        $location['state'] = $address['long_name'];
                    else if ($address['types'][0] == "postal_code")
                        $location['zipcode'] = $address['short_name'];
                }
                // Return all information of the location
                return $location;
            } else // Return error and cause
                return [
                    "cause" => ($type == "zipcode")? __('messages.error_location_zipcode') :  ( ($type == "current_location")? __('messages.error_current_location') : __('messages.error_location_addres') ),
                    "status" => $map_result['status']
                ];
        }

        // static public functions -------------------------------------------------------------------------
        /**
         * Get coordinates using my current location
         *
         * @return array
         */
        public static function getCurrentCoordinates(){
            // Return coordinates
            return self::getCoordinatesByUrl("https://www.googleapis.com/geolocation/v1/geolocate", "current_location", "POST", [
                "key" => env("GOOGLE_KEY", "")
            ]);
        }

        /**
         * Get the information of longitude and latitude
         *
         * @param string $address
         * @param string $type
         * @return array
         */
        public static function getCoordinates($address, $type = "zipcode") {
            // Url of Google Map
            $google_map_url = "https://maps.googleapis.com/maps/api/geocode/json?address=";
            // Know if it is zipcode
            $google_map_url .= ( ( ($type == "zipcode")? "{$address}" : str_replace(" ", "+", $address) ) . "&key=".env("GOOGLE_KEY", "") );
            // Return coordinates
            return self::getCoordinatesByUrl($google_map_url, $type);
        }

        /**
         * Get information of census block
         *
         * @param string $latitude
         * @param string $longitude
         * @return array
         */
        public static function getCensusBlock($latitude, $longitude) {
            // Url of FCC  
            $geo_fcc_url = "https://geo.fcc.gov/api/census/block/find?latitude=$latitude&longitude=$longitude&format=json";
            // Obtain the location information
            $fcc_result = json_decode(file_get_contents($geo_fcc_url), true);
            // Check if the information of census block exist
            if ($fcc_result['status'] == 'OK')
                return [
                    "status" => "OK",
                    'blockcode' => ($fcc_result["Block"]["FIPS"])
                ];
            else // Return error with cause
                return [ 
                    "cause" => __('messages.error_census_block'),
                    "status" => $fcc_result['status']
                ];
        }
    }