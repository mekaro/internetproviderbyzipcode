<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/zipcode/{code}', 'ProviderController@getProvidersByZipcode')->where(['code' => '^[0-9]{5}([0-9]{4})?$']);
Route::get('/address', 'ProviderController@getProvidersByLocation');
Route::get('/current-address', 'ProviderController@getProvidersByCurrentLocation');
Route::get('/all', 'ProviderController@getAllProviders');
